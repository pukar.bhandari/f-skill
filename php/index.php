<?php include_once "header.php"; ?>
    <div class="home">
    <!--    <div class="image-container">-->
    <!--        <img class="mySlides" src="../images/1.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/2.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/3.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/4.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/5.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/6.jpg" style="width:100%">-->
    <!--        <img class="mySlides" src="../images/7.jpg" style="width:100%">-->
    <!--    </div>-->
<!--        <img class="mySlides" src="../images/1.jpg">-->
        <div class="home-img">
            <p>Skills Training & Micro Enterprise Development</p>
        </div>
    </div>

    <div class="iNgo">
        <img class="iNgo1" src="../images/logos/UNDP.png" alt=""/>
        <img class="iNgo2" src="../images/logos/HELVETAS.png" alt=""/>
        <img class="iNgo3" src="../images/logos/SDCLogo.png" alt=""/>
        <img class="iNgo4" src="../images/logos/UN-Women-logo.png" alt=""/>
<!--        <img src="../images/logos/usaid-1.png" alt=""/>-->
<!--        <img src="../images/logos/winrock.png" alt=""/>-->
<!--        <img src="../images/logos/world-bank.png" alt=""/>-->
<!--        <img src="../images/logos/SAMI-Logo.jpg" alt=""/>-->
<!--        <img src="../images/logos/IOM-Logo.png" alt=""/>-->
<!--        <img src="../images/logos/nepal-govt.png" alt=""/>-->
    </div>

    <div class="who">
        <div class="wrapper clearfix">
            <div class="who left">
                <h2>WHO WE ARE</h2>
                <p>F-Skill Pvt. Ltd. started as a project of HELVETAS Nepal, funded by Swiss Agency for Development and Cooperation (SDC) in 2002. The project period lasted till 2006, after which F-Skill project transformed into F-Skill Pvt. Ltd.</p>
                <p>With the F-Skill project, the mobile training model was able to be replicated throughout Nepal through the franchising concept wherein private training providers were mobilized to deliver training as franchisees of F-Skill.</p>
                <p>While the franchising model continues till present, F-Skill has started implementing training services directly in all locations throughout Nepal. These include projects: EVENT/World Bank, EF-SR/HELVETAS, SABAL/Save the Children.</p>
            </div>

            <div class="who right">
                <img src="../images/IMG_9430.jpg" alt=""/>
            </div>
        </div>
    </div>

    <div class="trades">
        <h3 class="common">OUR TRADES</h3>
        <div class="trades-imgs">
            <div class="imgs imgs1">
                <p>Electrician</p>
            </div>

            <div class="imgs imgs2">
                <p>Scaffolding</p>
            </div>

            <div class="imgs imgs3">
                <p>Plumber</p>
            </div>

<!--            <div class="imgs imgs4">-->
<!--                <p>Hand woolen knitting</p>-->
<!--            </div>-->

            <div class="imgs imgs5">
                <p>Beauty Parlor</p>
            </div>

            <div class="imgs imgs6">
                <p>Tailoring</p>
            </div>
<!---->
<!--            <div class="imgs imgs7">-->
<!--                <p>Beads, Pearl and Crystal maker</p>-->
<!--            </div>-->
<!---->
<!--            <div class="imgs imgs8">-->
<!--                <p>Steel fixter</p>-->
<!--            </div>-->
<!---->
<!--            <div class="imgs imgs9">-->
<!--                <p>Mason</p>-->
<!--            </div>-->
<!---->
<!--            <div class="imgs imgs10">-->
<!--                <p>Garment</p>-->
<!--            </div>-->
        </div>
    </div>

    <div class="updates">
        <div class="update-menu">
            <h4>CASE STUDIES</h4>
            <h4>UPDATES</h4>
        </div>

        <div class="recent-updates">
            <h2>UPDATES</h2>
            <p>Our recent work/articles are featured in this section.</p>
            <h3 class="common">OFFICIAL UPDATES</h3>
        </div>

        <div class="official-updates wrapper">
            <div class="updates1 clearfix">
                <div class="updates-img left">
                    <img src="../images/fskill3-300x300.jpg" alt=""/>
                </div>

                <div class="updates-info right clearfix">
                    <h3>शक्ति समूह र एफ – स्किल प्रा. लि. विच साझेदारी</h3>
                    <p>शक्ति समूह र एफ – स्किल प्रा. लि. विच लामो समय देखि साझेदारीमा विभिन्न सीपमूलक तालिमहरु संचालन हुदै आएको छ । यसै सन्दर्भमा म्ँक्ष्म् को आर्थिक सहयोग र ऋजष्मिज्यउभ ग्प् को साझेदारीमा शक्ति समूहद्धारा संचालित ऋीब्ःए परियोजना अन्र्तगत सीपमूलक तालिम संचालनका लागि एफ – स्किललाई  साझेदारी संस्थाको रुपमा छनौट गरिएको थियो । उक्त परियोजना अन्र्तगत […]</p>
                    <a href="#" class="right">[ Read More ]</a>
                </div>
            </div>

            <div class="updates1 clearfix">
                <div class="updates-img left">
                    <img src="../images/fskill1-300x300.jpg" alt=""/>
                </div>

                <div class="updates-info right clearfix">
                    <h3>PARTNERSHIP WITH VOCATIONAL AND SKILL DEVELOPMENT TRAINING CENTRE, PROVINCE 5, NEPALGUNJ</h3>
                    <p>F-SKILL has started seven events of vocational and skill development training in Province 5 from 28 April 2019 in partnership with Province Government, Province 5, Ministry of Social Development, Vocational and Skill Development Training Centre, Nepalgunj. The trades/occupations include 1 x Fashion Designing (Level 2), 1 x Building Electrician (Level 2), 2 x Building Electrician […]</p>
                    <a href="#" class="right">[ Read More ]</a>
                </div>
            </div>

            <div class="updates1 clearfix">
                <div class="updates-img left">
                </div>

                <div class="updates-info right clearfix">
                    <h3>ADVANCED TAILORING TRAINING COMPLETED</h3>
                    <p>F-SKILL has successfully concluded the Advanced Tailoring Training (390 hours) with financial support from All Angels Nepal (AAN) to 18 female youths on 28 April 2019. In order to ensure that the trainees could attain a wide range of skill and exposure, they were practiced on electric (Industrial) sewing machines. Before this Advanced Training, the […]</p>
                    <a href="#" class="right">[ Read More ]</a>
                </div>
            </div>

            <div class="updates1 clearfix">
                <div class="updates-img left">
                    <img src="../images/Fifth-Batch-300x300.jpg" alt=""/>
                </div>

                <div class="updates-info right clearfix">
                    <h3>FIFTH BATCH OF GARMENT FABRICATION TRAINING COMPLETED</h3>
                    <p>F-SKILL has successfully completed the 5th Batch of Garment Fabrication Training (one month) funded by HELVETAS/Safer Migration (SaMi) Project on 19 March 2019 to 21 potential women migrant workers from throughout the country. F-SKILL has so far trained 107 participants in 2018/19, of which 45 trainees have already gone for foreign employment in Jordan.</p>
                    <a href="#" class="right">[ Read More ]</a>
                </div>
            </div>
        </div>
    </div>

<?php include_once "footer.php"; ?>