<?php include_once "header.php"; ?>
<div class="where-map">
    <img src="../images/district_map_nepal.png" alt=""/>
</div>
<div class="projects-header">
    <h1>ONGOING PROJECTS</h1>
    <p>Leading service provider in the technical and vocational education and training (TVET) sector of Nepal. More than
        30,000 persons from disadvantaged groups have been provided vocational training.</p>
</div>
<div class="projects">
    <div class="wrapper">
        <div class="projects-contain">
            <div class="project1">
                <div class="project-detail clearfix">
                    <h3>VOCATIONAL AND ENTREPRENEURSHIP DEVELOPMENT TRAINING</h3>
                    <p>Grounded in the vision of equality enshrined in the Charter of the United Nations, UN Women works
                        for the elimination of discrimination against women and girls; the empowerment of women; and the
                        achievement of equality between women and men as partners and beneficiaries of development,
                        human rights, humanitarian action and peace and security. Placing women’s […]</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="project-img">
                    <img src="../images/20864087_232802443913799_1599763019_n-920x568.jpg" alt=""/>
                </div>
            </div>
            <div class="project1 project2">
                <div class="project-detail clearfix">
                    <h3>SUSTAINABLE ACTION FOR RESILIENCE AND FOOD SECURITY (SABAL)</h3>
                    <p>Sabal is a five-year, $59 million project that works in 11 districts to improve food security and
                        nutrition outcomes at the individual, household, and community levels. Save the Children and its
                        partners implement a multi-sectoral project that includes activities relating to agriculture,
                        livelihood diversification, nutrition, and disaster risk reduction. Sabal is designed to address
                        the root causes of poverty with the aim of assisting individuals, households, and communities to
                        positively manage shocks and stresses related to natural disasters, climate change, political
                        unrest, as well as more localized shocks like illness or death in the family.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="project-img">
                    <img src="../images/IMG_20170407_094958_BURST1-920x568.jpg" alt=""/>
                </div>
            </div>
            <div class="project1">
                <div class="project-detail clearfix">
                    <h3>SAFER MIGRATION PROJECT (SAMI)</h3>
                    <p>SaMi is a bilateral initiative of the Government of Nepal and the Government of Switzerland. The
                        overall goal of SaMi is the safer and more beneficial migration for women and men, who choose to
                        go for migration. F-SKILL is in charge of providing vocational skills training’s to encourage
                        semi-skilled (rather than unskilled) migration and linking traines with job placement providers.
                        Additionally, F-SKILL provides counseling sessions on family management, legal aspects,
                        documents required for foreign employment, information on destination countries, money
                        management, and future planning for potential migrants.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
                <div class="project-img">
                    <img src="../images/SAMI-800L300P75Q-34-920x568.jpg" alt=""/>
                </div>
            </div>
        </div>
        <div class="load">
            <a href="#">LOAD MORE<br>
                <i class="fa fa-arrow-down"></i></a>
        </div>
    </div>
</div>

<div class="completed">
    <div class="wrapper">
        <div class="projects-header">
            <h1>COMPLETED PROJECTS</h1>
            <p>Through an innovative franchisee network, F-SKILL is able to deliver trainings throughout the country.</p>
        </div>
        <div class="completed-contain">
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/800L30075Q-15-copy-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>SKILLS TRAINING AND MICRO ENTERPRISE DEVELOPMENT TO VERIFIED MINORS & LATE RECRUITS (FORMER MAOIST COMBATANTS)</h3>
                    <p>The Main objective of the project was to train VMLRs discharged from the Maoists army cantonment as entrepreneurs and assist them establish their own micro-enterprises in order to have a sustainable rehabilitation in to civilian life.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/IMG_9527-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>EMPLOYMENT FUND PROJECT</h3>
                    <p>Employment Fund (EF) is a national level program implemented by HELVETAS Swiss Intercooperation Nepal which supports the skills training of poor and socially discriminated youths in order to link them to gainful employment.</p>
                    <p>in addition to technical skills training, F-SKILL provides life skills, literacy & numeracy and post training support for micro-enterprise creation.</p>
                    <p>Technical trainings in following sub-sectors were delivered, contributing to strengthened & diversified livelihoods</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/slider24-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>ENHANCED VOCATIONAL EDUCATION AND TRAINING PROJECT (EVENT)</h3>
                    <p>The main objective of this project is to expand the supply of skilled and employable labor by increasing access to quality training programs, and by strengthening the technical and vocational education and training system in Nepal. The project emphasizes in increasing access to technical education and vocational training (TEVT) programs for disadvantaged youth especially poor, living in lagging regions, female, dalit, marginalized janajatis and people with disability through targeting and other inclusive processes.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/800L300P7Q-15-copy-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>EDUCATION FOR INCOME GENERATION PROJECT NEPAL (EIG)</h3>
                    <p>The goal of the EIG project was to mitigate conflict by training targeted marginalized youth for employment in the Mid-Western Region of Nepal. F-SKILL was the implementing partner for Component 2 – Increased vocational training and employment opportunities for targeted youth.</p>
                    <p>F-SKIll implemented trainings in all districts of the Mid-West region.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/slider15-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>BENEFICIARIES LINKED TO EMPLOYMENT PROVIDERS OR SELF-EMPLOYMENT</h3>
                    <p>The main objective of the project was to train VMLRs discharged form the Maoists army cantonment as entrepreneurs and assist them establish their own micro-enterprises in order to have a sustainable rehabilitation in to civilian life.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="completed1">
                <div class="completed-img">
                    <img src="../images/slider22-920x568.jpg" alt=""/>
                </div>
                <div class="completed-details clearfix">
                    <h3>NSTB SKILL TEST CONDUCTED FOR PARTICIPANTS</h3>
                    <p>The main objective of the project was to train VMLRs discharged form the Maoists army cantonment as entrepreneurs and assist them establish their own micro-enterprises in order to have a sustainable rehabilitation in to civilian life.</p>
                    <a href="#" class="right">EXPLORE MORE <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="load">
            <a href="#">LOAD MORE<br>
                <i class="fa fa-arrow-down"></i></a>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>

