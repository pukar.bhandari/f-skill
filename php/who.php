<?php include_once "header.php"; ?>
<div class="home">
    <div class="home-img who-img">
        <p></p>
    </div>
</div>

<div class="who">
    <div class="who-first">
        <h1>WE ARE</h1>
        <p>Leading service provider in the technical and vocational education and training (TVET) sector of Nepal. More
            than 30,000 persons from disadvantaged groups have been provided vocational training.</p>
    </div>
    <div class="who-second wrapper">
        <div class="second-img">
            <img src="../images/3508L300P-6-of-11-min.jpg" alt=""/>
        </div>

        <div class="second-text">
            <p>F-SKILL Pvt. Ltd. started as a project of HELVETAS Nepal, founded by Swiss Agency for Development and
                Cooperation (SDC) in 2002. The project period lasted till 2006, after which F-SKILL project transformed
                into F-SKILL Pvt. Ltd.</p>
            <p>F-SKILL project was initiated by HELVETAS & SDC in 2002 to scale-up and replicate the short-term, mobile
                training models tied to employment linkage which proved successful with the SKILL Nepal Project.</p>
            <p>With the F-SKILL project , the mobile training model was able to be repliated throughout Nepal through
                the franchising concept wherein private training providers were mobilized to deliver training as
                franchises of F-SKILL.</p>
            <p>While the franchising model continues present, F-SKILL has started implementing training services
                directly in all locations throughout Nepal. These include projects: EVENT/World Bank, EF-SR/HELVETAS,
                SABAL/Save the Children.</p>
            <p>F-SKILL has been providing Training and Employment services since 2006. Pioneer in scaling up short-term
                training throughout Nepal Over 35,000 youths trained in 45 trades in all districts. Network of training
                for donor agencies, NGOs and Nepal government agencies: Employment Fund (EF) & Safer Migration Project
                (SaMi)/HELVETAS Swiss in cooperation Nepal, UN WOMEN, USAID, Winrock, IOM, World Vision, PACT &
                Education for Income Generation Project (EIG)/USAID, Training for VMLRs (former combatants)/UNIRP.</p>
        </div>
    </div>

    <div class="who-third">
        <p>F-SKILL’s goal is to become the leader in providing high quality vocational training and will continue to
            strive to be a leading service provider in Nepal’s TVET sector. F-SKILL will develop new products and
            innovative approaches to training by working with local partners and international organizations. F-SKILL
            will continue to provide high quality counselling and soft-skills to training graduates, potential migrants
            and returnee migrants.</p>
    </div>

    <div class="who-fourth wrapper">
        <div class="who-fourth-first">
            <h3>VISION</h3>
            <p>F-SKILL will be recognized as the leading organization providing employment oriented technical and
                vocational training for disadvantaged youth in Nepal.</p>
        </div>

        <div class="who-fourth-first">
            <h3>MISSION</h3>
            <p>To provide livelihood opportunities for disadvantaged youth.</p>
        </div>

        <div class="who-fourth-first">
            <h3>GOAL</h3>
            <p>The base of success of F-SKILL is the higher rate of employment of the training graduates.</p>
        </div>

        <div class="who-fourth-first">
            <h3>OBJECTIVE</h3>
            <p>Provide vocational skills training and employment, soft skills trainings, enterprise development programs
                and training packages development.</p>
        </div>
    </div>


    <div class="who-fifth">
        <div class="wrapper">
            <h3 class="common">OUR SERVICES</h3>
            <div class="who-fifth-container">
                <div class="who-fifth-first">
                    <img src="../images/training.png" alt=""/>
                    <p>Vocational Skills Training And Employment</p>
                </div>

                <div class="who-fifth-first">
                    <img src="../images/soft-skill.png" alt=""/>
                    <p>Soft Skills Trainings</p>
                </div>

                <div class="who-fifth-first">
                    <img src="../images/suitcase.png" alt=""/>
                    <p>Enterprise Development Programs</p>
                </div>

                <div class="who-fifth-first">
                    <img src="../images/package.png" alt=""/>
                    <p>Training Packages Development</p>
                </div>
            </div>
        </div>
    </div>

    <div class="who-sixth">
        <div class="wrapper clearfix">
            <div class="who-sixth-list left">
                <p class="bold">CORE</p>
                <p class="bolder">PRINCIPLES</p>
                <ul>
                    <li>Focus on employment</li>
                    <li>Poor and disadvantaged groups prioritized in participant selection</li>
                    <li>Development and regular updates of training packages</li>
                    <li>Demand-driven, market-led mobile training</li>
                    <li>Social aspects included in all training: gender, HIV/AIDS, reproductive health, legal aid and
                        labor rights
                    </li>
                    <li>Entrepreneurial skills included in training course</li>
                    <li>Income verification of training participants before and after training</li>
                    <li>Outcome based financing</li>
                    <li>During and post training monitoring</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="who-seventh">
        <div class="wrapper">
            <h3>MORE ABOUT F-SKILL</h3>
            <div class="who-seventh-content">
                <div class="who-seventh-1">
                    <h4>F-SKILL’S CORE BUSINESS</h4>
                    <p>F-SKILL’s core business is delivering quality employment-oriented training to economically poor
                        and socially discriminated groups, specially Dalits, Janajatis, women and youth affected by the
                        conflict.</p>
                </div>
                <div class="who-seventh-1">
                    <h4>SOCIAL AND GENDER-INCLUSIVE APPROACH</h4>
                    <p>Poor participants from disadvantaged groups – Dalits, Janajatis, women and conflict affected –
                        are given priority in participant selection.</p>
                </div>
                <div class="who-seventh-1">
                    <h4>PROJECT PHASE SUCCESS</h4>
                    <p>Following the projet phase of F-SKILL (2002 – 2006), over 86% of F-SKILL trainees were found in
                        gainful employment after six month of training completion. An independent tracer study conducted
                        in 2007 confirmed the hight employment rate.</p>
                </div>
                <div class="who-seventh-1">
                    <h4>DIFFERENTIAL PRICING APPROACH FOR PARTICIPANT SELECTION</h4>
                    <p>F-SKILL pays its franchiesees higher incentives for training participants from most discriminated
                        and conflict affected groups.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="who-eighth">
        <div class="wrapper">
            <h3>OUR <strong>TEAM</strong></h3>
            <p>To ensure the quality of the projects, FSKILL relies on the consulting services of Philanthropy Advisors,
                a strategic philanthropy consulting firm established by humanitarian professionals</p>
            <div class="team-members clearfix">
                <div class="member1 clearfix">
                    <div class="member-img">
                        <img src="../images/members/personalImage1.jpg" alt=""/>
                    </div>
                    <div class="member-details">
                        <h4>DR. SUNIT ADHIKARI</h4>
                        <p>Executive Director</p>
                    </div>
                </div>
                <div class="member2 clearfix">
                    <div class="member-img">
                        <img src="../images/members/personalImage2.jpg" alt=""/>
                    </div>
                    <div class="member-details">
                        <h4>AMSHER KC</h4>
                        <p>Health Counselor</p>
                    </div>
                </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage3.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>SHIVA RANA BHATT</h4>
                    <p>District Coordinator - Nepalgunj</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage4.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>ER. AFTAB HASHMI</h4>
                    <p>Civil Engineer</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage5.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>BITRA RAI</h4>
                    <p>Psychological Counselor</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage6.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>LAXMI THAKURI</h4>
                    <p>Cook & Cleaner</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage7.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>ANURADHA SHRESTHA</h4>
                    <p>Finance & Admin Officer</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage8.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>ASMITA PRASAI</h4>
                    <p>Finance Assistant</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage9.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>SAMITA SHARMA</h4>
                    <p>Health and Life Skill Counselor</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage10.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>RUKESH GHIMIRE</h4>
                    <p>District Coordinator - Dhading</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage11.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>NIRUPAMA GHIMIRE</h4>
                    <p>Program Officer</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage12.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>YUNUSHA KAFLE PAUDEL</h4>
                    <p>Program Associate</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage13.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>KUM BAHADUR GURUNG</h4>
                    <p>Program Manager</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage14.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>SUSHANT HARSHA BAJRACHARYA</h4>
                    <p>Business Development Coordinator</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage15.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>PRAMILA LAMA</h4>
                    <p>Counselor</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage16.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>SANGEETA TAMANG</h4>
                    <p>Social Development Officer</p>
                </div>
            </div>
            <div class="member1 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage17.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>NUTAN SHAH</h4>
                    <p>Front Desk Officer</p>
                </div>
            </div>
            <div class="member2 clearfix">
                <div class="member-img">
                    <img src="../images/members/personalImage18.jpg" alt=""/>
                </div>
                <div class="member-details">
                    <h4>KANCHA TAMANG</h4>
                    <p>Security Guard</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once "footer.php"; ?>
