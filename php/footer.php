<div class="footer">
    <div class="quote">
        <p>The humility to see the world as it is the audacity to imagine the world as it could be</p>
    </div>
    <div class="location">
        <p><i class="fa fa-map-marker"></i> OUR LOCATION</p>
    </div>

    <div class="map">
        <iframe width="100%" height="450" frameborder="0" style="border: 0px; pointer-events: none;" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJjzJdOSsY6zkRRnC1tkTpNnI&amp;key=AIzaSyB6AoRb93xwga4Aj7A-bUyrde8ywjPpKRo" allowfullscreen=""></iframe>
    </div>

    <div class="footer-nav">
        <div class="footer-nav-logo">
            <img src="../images/site-logo.svg" width="100" height="100" alt=""/>
        </div>

        <div class="footer-nav-nav">
            <h3>About Us</h3>
            <ul>
                <li><a href="#">HOME</a></li>
                <li><a href="#">WHO WE ARE</a></li>
                <li><a href="#">HOW WE WORK</a></li>
                <li><a href="#">WHERE WE WORK</a></li>
                <li><a href="#">CONTACT</a></li>
            </ul>
        </div>

        <div class="footer-contact">
            <h3>Contact</h3>
            <p>F-SKILL PVT. LTD.,</p>
            <p><i class="fa fa-map-marker"></i> Khumaltar, Lalitpur, Nepal (Opposite of Premier School)</p>
            <p><i class="fa fa-phone"></i> +977-1-5555301, 5524648,</p>
            <p><i class="fa fa-envelope-o"></i> fskill@fskill.org.np, mail.fskill@gmail.com</p>
        </div>

        <div class="footer-social">
            <h3>Join Our Mailing List</h3>
            <input id="email" type="email" placeholder="Sign up for our mailing list"/>
            <label for="email">Join Us</label>
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="mailto:fskill@fskill.org.np" target="_blank"><i class="fa fa-envelope-o"></i></a></li>
            </ul>
        </div>
    </div>

    <div class="copyright clearfix">
        <div class="left">
            <p>Copyright 2017, F-SKILL Pvt. Ltd. All rights reserved. A registered trademark of the FSKILL Foundation.</p>
        </div>
        <div class="right">
            <ul>
                <li class="privacy"><a href="#">Privacy Policy</a></li>
                <li class="legal"><a href="#">Legal</a></li>
                <li><a href="#">Financial Information</a></li>
            </ul>
        </div>
    </div>

    <div class="developer">
        <p>Developed by</p>
        <img src="../images/studiomatrix.svg" width="200" height="25" alt=""/></li>
    </div>
</div>

<script>
    (function ($) {
        $('.hamburger').on('click', function () {
            console.log('clicked')
            $('.nav').toggleClass('open');
        })
        $('.hamburger').on('click', function () {
            console.log('clicked')
            $('.hamburger').toggleClass('open');
        })
        <?php
        $file = basename($_SERVER['PHP_SELF']);
        if($file == 'index.php'||$file == 'who.php'||$file == 'contact.php'){
            sNav();
        }
        function sNav()
        {
        ?>
        $(document).ready(function(){
            $(window).scroll(function(){
                var scroll = $(window).scrollTop();
                if (scroll > 80) {
                    $(".header").css("background" , "#000");
                }
                else{
                    $(".header").css("background" , "none");
                }
            })
        })
        <?php
        }
        ?>

        <?php
        $file = basename($_SERVER['PHP_SELF']);
        if($file == 'how.php'||$file == 'where.php'){
            fNav();
        }
        function fNav()
        {
        ?>
        $(document).ready(function(){
            $(".header").css("background" , "#000");
        })
        <?php
        }
        ?>
    })(jQuery)
</script>

</body>
</html>