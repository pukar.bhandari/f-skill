<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 5</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="../js/jquery.js"></script>
</head>
<body>

<div class="header clearfix">
    <div class="ham clearfix">
        <div class="hamburger">
            <div id="top-bun"></div>
            <div id="patty"></div>
            <div id="bottom-bun"></div>
        </div>
    </div>
        <div class="logo left">
            <a href="index.php"><img src="../images/site-logo.svg" alt="" width="75" height="75"/></a>
        </div>

        <div class="nav right">
            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="who.php">WHO WE ARE</a></li>
                <li><a href="how.php">HOW WE WORK</a></li>
                <li><a href="where.php">WHERE WE WORK</a></li>
                <li><a href="contact.php">CONTACT</a></li>
            </ul>
        </div>
</div>
<?php
$file = basename($_SERVER['PHP_SELF']);
if($file == 'index.php'){
    subNav();
}
function subNav()
{
?>
<div class="sticky-footer clearfix">
    <div class="left"><p>Software Developer Training With Level 4 Apprenticeship</p></div>
    <div class="right"><button>LEARN MORE</button></div>
</div>
<?php
}
?>