<?php include_once "header.php"; ?>
<div class="how-video">
    <div class="wrapper">
        <iframe width="100%" height="700" src="https://www.youtube.com/embed/9z0PwidRG8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="how-work">
    <h1>HOW <strong>WE WORK</strong></h1>
    <p>F-SKILL continualy creates and updates curriculums and training courses, following labour market needs. F-SKILL has provided training services in over 45 occupations throughout Nepal.</p>
</div>

<div class="steps">
    <div class="wrapper">
        <div class="workflow">
            <div class="step">
                <div class="step-text">
                    STEP 1
                </div>
                <div class="step-icon">
                    <img src="../images/program-design.png" alt=""/>
                </div>
                <div class="step-details">
                    <h3>PROGRAM DESIGN</h3>
                    <ul>
                        <li>Program Design</li>
                        <li>Participant Selection</li>
                    </ul>
                </div>
            </div>

            <div class="step">
                <div class="step-text">
                    STEP 2
                </div>
                <div class="step-icon">
                    <img src="../images/market-assessment.png" alt=""/>
                </div>
                <div class="step-details">
                    <h3>MARKET ASSESSMENT</h3>
                    <ul>
                        <li>Rapid Market Assessment</li>
                        <li>Curriculum Design</li>
                        <li>Career Counselling</li>
                    </ul>
                </div>
            </div>

            <div class="step">
                <div class="step-text">
                    STEP 3
                </div>
                <div class="step-icon">
                    <img src="../images/training-blue.png" alt=""/>
                </div>
                <div class="step-details">
                    <h3>TRAINING</h3>
                    <ul>
                        <li>Training Preparation</li>
                        <li>Technical Skill Training</li>
                        <li>Soft Skill Training</li>
                        <li>Skill Test</li>
                    </ul>
                </div>
            </div>
            <div class="step">
                <div class="step-text">
                    STEP 4
                </div>
                <div class="step-icon">
                    <img src="../images/employment.png" alt=""/>
                </div>
                <div class="step-details">
                    <h3>EMPLOYMENT</h3>
                    <ul>
                        <li>Linkage with Employers</li>
                        <li>Business Support</li>
                        <li>Income Verification</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>