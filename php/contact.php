<?php include_once "header.php"; ?>
    <div class="home">
        <div class="home-img contact-img">
            <p>Have a question?<br>Want to get involved?</p>
        </div>
    </div>

    <div class="who-first info-first">
        <h1>CONTACT FSKILL</h1>
        <p>Fill out the form below with your donation question, partnership idea, media request or other inquiry and
            someone from The Climate Reality Project will get back to you.</p>
    </div>

    <div class="info">
        <div class="wrapper">
            <div class="info-container">
                <div class="form-input">
                    <h2 class="common">GET IN TOUCH</h2>
                    <form id="survey-form">
                        <input id="first-name" type="text" placeholder="First Name"/>
                        <input id="last-name" type="text" placeholder="Last Name"/>
                        <input id="email" type="email" placeholder="E-mail"/>
                        <input id="phone" type="tel" placeholder="Phone No."/>
                        <input id="subject" type="text" placeholder="Subject"/>
                        <input id="message" type="text" placeholder="Message"/>
                        <button><a href="#">SEND</a></button>
                    </form>
                </div>

                <div class="more-info">
                    <h3 class="common">MORE INFO</h3>
                    <div class="info-location">
                        <h4>HEAD OFFICE</h4>
                        <p><i class="fa fa-map-marker"></i> Khumaltar, Lalitpur, Nepal</p>
                        <p><i class="fa fa-phone"></i> +977-1-5555301, +977-1-5524648</p>
                        <p><i class="fa fa-envelope-o"></i> fskill@fskill.org.np, mail.fskill@gmail.com</p>
                        <p><i class="fa fa-facebook"></i> https://facebook.com/fskillorg</p>
                        <button><a href="#">VIEW OUR LOCATION</a></button>
                    </div>

                    <div class="branch-location">
                        <h4>REGIONAL FIELD OFFICES</h4>
                        <div class="regional">
                            <div class="regional1">
                                <p><i class="fa fa-map-marker"></i> Dhangadi, Nepal</p>
                                <p><i class="fa fa-phone"></i> +977-9851201169</p>
                            </div>
                            <div class="regional1">
                                <p><i class="fa fa-map-marker"></i> Nepalgunj, Nepal</p>
                                <p><i class="fa fa-phone"></i> +977-081-551713</p>
                            </div>
                            <div class="regional1">
                                <p><i class="fa fa-map-marker"></i> Okhaldhunga</p>
                                <p><i class="fa fa-phone"></i> +977-9842931151</p>
                            </div>
                            <div class="regional1">
                                <p><i class="fa fa-map-marker"></i> Ramechhap, Nepal</p>
                                <p><i class="fa fa-phone"></i> +977-048-540184</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once "footer.php"; ?>